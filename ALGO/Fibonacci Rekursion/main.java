package Fibonacci;

import java.util.Scanner;

public class main
{
	private static Scanner s;
	private static int[] arr;
	
	public static void main(String[] args)
	{
		init();
		writeFib(arr);
		printFib(arr);
	}
	
	//Für das befüllen eines Arrays sehr ineffizient, besser die Summe aus den vorherigen Elementen ohne Rekursion bilden!!!
	public static int fibonacci(int stelle)
	{
		    if(stelle == 0)
		        return 0;
		    else if(stelle == 1)
		      return 1;
		   else
		      return fibonacci(stelle - 1) + fibonacci(stelle - 2);
		
	}
	public static void init()
	{
		s = new Scanner(System.in);
		System.out.println("Bitte geben Sie an bis zu welcher Stelle sie die Fibonacci Folge berechnen wollen");
		int i = s.nextInt();
		arr = new int[i];
	}
	
	public static void writeFib(int[] arr)
	{
		for(int i = 0;i<arr.length;i++)
		{
			arr[i]=fibonacci(i);
		}
	}
	
	public static void printFib(int[] arr)
	{
		for(int i = 0;i<arr.length;i++)
		{
			System.out.println("Stelle " + i + ":\t" + arr[i]);
		}
	}
}
