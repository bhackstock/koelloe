package DocumentBuilder;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class main {

	public static void main(String argv[]) {

	  try {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("Kontinent");
		rootElement.setAttribute("name", "Europa");
		doc.appendChild(rootElement);

		Element land = doc.createElement("Land");
		rootElement.appendChild(land);
		land.setAttribute("name", "Oesterreich");

		Element bundesland = doc.createElement("Bundesland");
		bundesland.setAttribute("name", "Tirol");
		land.appendChild(bundesland);

		Element einwohner = doc.createElement("Einwohner");
		einwohner.appendChild(doc.createTextNode("1 Million"));
		bundesland.appendChild(einwohner);
		
		Element landschaft = doc.createElement("Landschaft");
		landschaft.appendChild(doc.createTextNode("Bergig"));
		bundesland.appendChild(landschaft);
		
		Element bundesland2 = doc.createElement("Bundesland");
		bundesland2.setAttribute("name", "Wien");
		land.appendChild(bundesland2);

		Element einwohner2 = doc.createElement("Einwohner");
		einwohner2.appendChild(doc.createTextNode("2 Millionen"));
		bundesland2.appendChild(einwohner2);
		
		Element landschaft2 = doc.createElement("Landschaft");
		landschaft2.appendChild(doc.createTextNode("Flach"));
		bundesland2.appendChild(landschaft2);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("C:/Users/benni/Dropbox/HTL/BIBI/XML - K�ll�/XMLDocBuilder.xml"));

		transformer.transform(source, result);
		
	  } catch(Exception e){e.printStackTrace();}
	}
}
