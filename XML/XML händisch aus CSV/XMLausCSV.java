package CSV;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class XMLausCSV
{
	private static BufferedReader reader;
	private static FileWriter writer;
	private static String einrueckTiefe = "";
	private static boolean kontinentOffen = false;
	private static boolean landOffen = false;
	private static boolean bunLandOffen = false;

	public static void main(String[] args) throws IOException
	{
		reader = new BufferedReader(
				new FileReader(new File("C:/Users/benni/Dropbox/HTL/BIBI/XML - K�ll�/Programm1.csv")));
		writer = new FileWriter(new File("C:/Users/benni/Dropbox/HTL/BIBI/XML - K�ll�/XML.xml"));

		writeXMLHeader("1.0", "UTF-8");
		writeCSVtoXML();
		writer.flush();
	}

	public static void writeXMLHeader(String version, String encoding) throws IOException
	{
		writer.write("<? xml version=\"" + version + "\" encoding=\"" + encoding + "\" ?>\n");
		writer.flush();
	}

	public static void writeCSVtoXML() throws IOException
	{
		String line;
		String[] gesplitted;
		while ((line = reader.readLine()) != null)
		{
			int index = 0;
			gesplitted = line.split(";");
			closeTagIfNecessary(gesplitted[index]);
			writer.write("\n" + einrueckTiefe + "<" + gesplitted[index] + " ");
			index++;
			A: while (true)
			{
				try
				{
					writer.write(gesplitted[index] + "=\"" + gesplitted[index + 1] + "\" ");
					index = index + 2;
					setTagOpened(gesplitted[0]);
				} catch (IndexOutOfBoundsException e)
				{
					if (gesplitted[0].equals("Bundesland"))
					{
						writer.write("/>");
					} else
					{
						writer.write(">");
					}
					break A;
				}
			}
		}
		closeAllTagsIfNecessary();
	}

	private static void closeAllTagsIfNecessary() throws IOException
	{

		if(landOffen)
		{
			writer.write("\n\t</Land>\n");
			landOffen = false;
		}
		if (kontinentOffen)
		{
			writer.write("\n</Kontinent>\n\n");
			kontinentOffen = false;
		}
	}

	private static void setTagOpened(String string) throws IOException
	{
		switch (string)
		{
		case "Kontinent":
		{
			if (!kontinentOffen)
			{
				kontinentOffen = true;
				einrueckTiefe = "\t";
			}
			break;
		}
		case "Land":
		{
			if (!landOffen)
			{
				landOffen = true;
				einrueckTiefe = "\t\t";
			}
			break;
		}
		case "Bundesland":
		{
			if (!bunLandOffen)
			{
				bunLandOffen = true;
			}
			break;
		}
		}
	}

	public static void closeTagIfNecessary(String name) throws IOException
	{
		switch (name)
		{
		case "Kontinent":
		{
			if (kontinentOffen)
			{
				if(landOffen)
				{
					writer.write("\n\t</Land>\n");
					landOffen = false;
				}
				einrueckTiefe = "";
				writer.write("\n</Kontinent>\n\n");
				kontinentOffen = false;
			}
			break;
		}
		case "Land":
		{
			if (landOffen)
			{
				einrueckTiefe = "\t";
				writer.write("\n\t</Land>\n");
				landOffen = false;
			}
			break;
		}

		}
	}
}
