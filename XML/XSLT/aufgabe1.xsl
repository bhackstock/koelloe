<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>
				<h2>Tabelle mit Kontinenten</h2>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th style="text-align:left">Kontinent</th>
						<th style="text-align:left">Land</th>
						<th style="text-align:left">Bundesland</th>
						<th style="text-align:left" colspan="2">Sonstiges</th>
					</tr>
					<xsl:for-each select="Kontinent">
						<xsl:for-each select="Land">
							<xsl:for-each select="Bundesland">
								<tr>
									<td>
										<xsl:value-of select="../../@name" />
									</td>
									<td>
										<xsl:value-of select="../@name" />
									</td>
									<td>
										<xsl:value-of select="@name"></xsl:value-of>
									</td>
									<xsl:for-each select="*">
										<td>
											<xsl:value-of select="." />
										</td>
									</xsl:for-each>
								</tr>
							</xsl:for-each>

						</xsl:for-each>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>

