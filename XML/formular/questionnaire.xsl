<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">

		<html>
			<body>
				<button form="form1" type="submit" id="absenden">Absenden</button>
				<span style="color: green">
					<h1>Online Fragebogen für Facebook</h1>
				</span>

				<form id="form1" method="post"
					action="http://www.hashemian.com/tools/form-post-tester.php">
					<xsl:for-each select="/*/*">
						<xsl:variable name="name" select="name()" />

						<xsl:choose>
							<xsl:when test="$name = 'closedEndedQ'">
								<h3>
									<xsl:value-of select="position()-1"></xsl:value-of>
									.
									<xsl:value-of select="./text/text()">
									</xsl:value-of>
								</h3>
								<xsl:value-of select="./qdescription/text()"></xsl:value-of>
								<fieldset style="border:0">
									<xsl:variable name="multiple" select="./choices/@mult" />
									<xsl:choose>
										<xsl:when test="$multiple = 'true'">
											<xsl:for-each select="./choices/choice">
												<ul>
													<label>
														<xsl:variable name="desc" select="./text/text()"></xsl:variable>
														<input type="checkbox" name="something" value="{$desc}" />
														<xsl:value-of select="$desc"></xsl:value-of>
													</label>
												</ul>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>

											<xsl:for-each select="./choices/choice">
												<ul>
													<label>
														<xsl:variable name="desc" select="./text/text()"></xsl:variable>
														<input type="radio" name="something" value="{$desc}" />
														<xsl:value-of select="$desc"></xsl:value-of>
													</label>
												</ul>
											</xsl:for-each>
										</xsl:otherwise>
									</xsl:choose>
								</fieldset>
							</xsl:when>

							<xsl:when test="$name = 'openendedMatrixQ'">
								<h3>
									<xsl:value-of select="position()-1"></xsl:value-of>
									.
									<xsl:value-of select="./text/text()">
									</xsl:value-of>
								</h3>
								<xsl:value-of select="./qdescription/text()"></xsl:value-of>
								<fieldset style="border:0">
									<table>
										<xsl:for-each select="./questions/question">
											<tr>
												<xsl:variable name="desc" select="./text/text()" />
												<td>
													<xsl:value-of select="$desc"></xsl:value-of>
												</td>
												<td>
													<input type="text" name="{$desc}" value=""></input>
												</td>
											</tr>
										</xsl:for-each>
									</table>
								</fieldset>
							</xsl:when>

							<xsl:when test="$name = 'questionMatrixMult'">
								<h3>
									<xsl:value-of select="position()-1"></xsl:value-of>
									.
									<xsl:value-of select="./text/text()">
									</xsl:value-of>
								</h3>

								<table border="1">
									<tr>
										<td></td>
										<xsl:for-each select="./choices/choice">
											<td>
												<xsl:value-of select="./text/text()"></xsl:value-of>
											</td>
										</xsl:for-each>
									</tr>
									<xsl:variable name="anzSpalten" select="count(./choices/choice)" />
									<xsl:for-each select="./questions/question">
										<tr>
											<xsl:variable name="attr" select="./text/text()" />
											<td>
												<xsl:value-of select="$attr" />
											</td>

											<xsl:for-each select="../../choices/choice">
												<td>
													<input type="radio" name="something" value="{$attr}" />
												</td>
											</xsl:for-each>
										</tr>
									</xsl:for-each>
								</table>
							</xsl:when>
							<xsl:otherwise>
							</xsl:otherwise>

						</xsl:choose>

					</xsl:for-each>
				</form>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>